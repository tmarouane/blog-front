import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private connectedUser: User = null;

  constructor(private authenticationService: AuthenticationService) {
    // On fait un subscrine pour changer la valeur de la variable "connectedUser" quand un utilisateur se connecte
    this.authenticationService.getLoggedUser.subscribe(data => this.connectedUser = data);
  }

  ngOnInit() {
    if (this.authenticationService.isConnected) {
      this.authenticationService.getConnectedUser().subscribe(
        data => this.connectedUser = data,
        error => console.error(error)
      );
    }
  }

  signout() {
    this.authenticationService.logout();
  }

}
