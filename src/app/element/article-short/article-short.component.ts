import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/model/article';
import { CommentService } from 'src/app/service/comment.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-article-short',
  templateUrl: './article-short.component.html',
  styleUrls: ['./article-short.component.css']
})
export class ArticleShortComponent implements OnInit {
  @Input() article: Article;
  private showComments: boolean = false;

  constructor(private commentService: CommentService, private userService: UserService) { }

  ngOnInit() {
  }

  getComments(article: Article = null): void {
    this.toggleShowComments();
    if (article.comments.length > 0 && this.showComments) {
      console.log(this.showComments);
      article.comments.forEach((comment, indexComment) => {
        this.commentService.getComment(comment.id).subscribe(
          data => {
            this.article.comments[indexComment] = data;
            this.userService.getUser(data.user.id).subscribe(
              data => this.article.comments[indexComment].user = data,
              error => console.error(error)
            )
          },
          error => console.error(error)
        );
      });
    }
  }

  toggleShowComments(): void {
    this.showComments = !this.showComments;
  }

}
