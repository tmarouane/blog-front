import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/model/article';
import { CommentService } from 'src/app/service/comment.service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { User } from 'src/app/model/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Comment } from 'src/app/model/comment';
import { ArticleService } from 'src/app/service/article.service';

@Component({
  selector: 'app-article-long',
  templateUrl: './article-long.component.html',
  styleUrls: ['./article-long.component.css']
})
export class ArticleLongComponent implements OnInit {
  @Input() article: Article;
  private connectedUser: User = null;
  commentForm: FormGroup;
  comment: Comment = null;
  emailInvalidErrorMessage: string = "Veuillez introduire une adresse mail valide";

  constructor(private fb: FormBuilder, private authenticationService: AuthenticationService, private commentService: CommentService, private articleService: ArticleService) {
    // On fait un subscrine pour changer la valeur de la variable "connectedUser" quand un utilisateur se connecte
    this.authenticationService.getLoggedUser.subscribe(data => this.connectedUser = data);

    this.commentForm = fb.group({
      content: [
        null,
        Validators.compose([
          Validators.minLength(3),
          Validators.maxLength(500)
        ])
      ]
    });

    this.commentForm.valueChanges.pipe(debounceTime(500)).subscribe(data => {
      this.prepareSaveComment()
    });
  }

  ngOnInit() {
    if (this.authenticationService.isConnected) {
      this.authenticationService.getConnectedUser().subscribe(
        data => this.connectedUser = data,
        error => console.error(error)
      );
    }
  }

  commentFN(): void {
    this.comment = this.prepareSaveComment();
    if (!this.prepareSaveComment()) {
      return;
    }
    
    this.commentService.addComment(this.comment).subscribe(
      data => {
        console.log(data);
        this.article.comments.push(this.comment);
        this.articleService.updateArticle(this.article).subscribe(
          data => console.log("backend updated"),
          error => console.error(error)
        );
      },
      error => console.error(error)
    );
  }

  prepareSaveComment(): Comment {
    const saveComment: Comment = {
      id: this.guid(),
      content: this.commentForm.controls["content"].value as string,
      user: {
        id: this.connectedUser.id
      },
      articleId: this.article.id,
      created: new Date(),
      modified: new Date()
    };
    return saveComment;
  }

  private guid() {
    return Math.floor((1 + Math.random()) * 0x10000)
  }

}
