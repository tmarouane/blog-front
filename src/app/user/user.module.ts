import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserSigninComponent } from './user-signin/user-signin.component';
import { UserSignupComponent } from './user-signup/user-signup.component';
import { SharedModule } from '../shared.module';
import { UserProfileComponent } from './user-profile/user-profile.component';

const routes: Routes = [
  { path: "signin", component: UserSigninComponent, pathMatch: "full" },
  { path: "signup", component: UserSignupComponent, pathMatch: "full" },
  { path: "profile/:id", component: UserProfileComponent, pathMatch: "full" }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [UserSigninComponent, UserSignupComponent, UserProfileComponent]
})
export class UserModule { }
