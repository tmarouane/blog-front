import { Component, OnInit } from '@angular/core';
import { Article } from '../model/article';
import { ArticleService } from '../service/article.service';
import { UserService } from '../service/user.service';
import { ActivatedRoute } from '@angular/router';
import { CommentService } from '../service/comment.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  private urlId: number = null;
  private article: Article;

  constructor(private route: ActivatedRoute, private articleService: ArticleService, private userService: UserService, private commentService: CommentService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.urlId = Number(params.get("id"));
    })

    this.articleService.getArticle(this.urlId).subscribe(
      data => {
        this.article = data
        console.log(this.article.comments);
        // Récupération de l'utilisateur créateur de l'article
        this.userService.getUser(this.article.user.id).subscribe(
          data => this.article.user = data,
          error => console.error(error)
        );
        // Récupération des commentaires de l'article
        this.article.comments.forEach((comment, indexComment) => {
          this.commentService.getComment(comment.id).subscribe(
            data => {
              this.article.comments[indexComment] = data;
              this.userService.getUser(data.user.id).subscribe(
                data => this.article.comments[indexComment].user = data,
                error => console.error(error)
              )
            },
            error => console.error(error)
          );
        });
      },
      error => console.error(error)
    )
  }

}
