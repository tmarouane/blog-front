import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArticleShortComponent } from './element/article-short/article-short.component';
import { ArticleLongComponent } from './element/article-long/article-long.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommentComponent } from './element/comment/comment.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ArticleShortComponent,
    ArticleLongComponent,
    CommentComponent
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ArticleShortComponent,
    ArticleLongComponent,
    CommentComponent
  ]
})
export class SharedModule { }