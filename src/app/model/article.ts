import { User } from "./user";
import { Comment } from "./comment";

export class Article {
    id?: number;
    title?: string;
    content?: string;
    img?: string;
    user?: User;
    comments?: Array<Comment>;
    created?: Date;
    modified?: Date;

    constructor(id?: number, title?: string, content?: string, img?: string, user?: User, comments?: Array<Comment>, created?: Date, modified?: Date) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.img = img;
        this.user = user;
        this.comments = comments;
        this.created = created;
        this.modified = modified;
    }
}
