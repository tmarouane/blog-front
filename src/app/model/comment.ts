import { User } from "./user";

export class Comment {
    id?: number;
    content?: string;
    user?: User;
    articleId: number;
    created?: Date;
    modified?: Date;

    constructor(id?: number, content?: string, user?: User, articleId?: number, created?: Date, modified?: Date) {
        this.id = id;
        this.content = content;
        this.user = user;
        this.articleId = articleId;
        this.created = created;
        this.modified = modified;
    }
}
