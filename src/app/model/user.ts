export class User {
    id?: number;
    email?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    created?: Date;
    modified?: Date;

    constructor(id?: number, email?: string, password?: string, firstName?: string, lastName?: string, created?: Date, modified?: Date) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.created = created;
        this.modified = modified;
    }
}
