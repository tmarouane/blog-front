import { Component, OnInit } from '@angular/core';
import { Article } from '../model/article';
import { ArticleService } from '../service/article.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private articles = Array<Article>();

  constructor(private articleService: ArticleService, private userService: UserService) { }

  ngOnInit() {
    this.articleService.getArticles().subscribe(
      data => {
        this.articles = data
        this.articles.forEach(element => {
          this.userService.getUser(element.user.id).subscribe(
            data => element.user = data,
            error => console.error(error)
          );
        });
      },
      error => console.error(error)
    )
  }

}
