import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const articles = [
      {
        id: 1,
        title: "What is Lorem Ipsum ?",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        img: "assets/article/coffee.jpg",
        user: {
          id: 1
        },
        comments: [
          {
            id: 1
          },
          {
            id: 2
          }
        ],
        created: new Date(),
        modified: new Date()
      },
      {
        id: 2,
        title: "What is Lorem Ipsum ?",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        img: "assets/article/coffee.jpg",
        user: {
          id: 2
        },
        comments: [],
        created: new Date(),
        modified: new Date()
      },
      {
        id: 3,
        title: "What is Lorem Ipsum ?",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        img: "assets/article/coffee.jpg",
        user: {
          id: 1
        },
        comments: [],
        created: new Date(),
        modified: new Date()
      }
    ];

    const users = [
      {
        id: 1,
        email: "marouane.terai@ynov.com",
        password: "1234",
        firstName: "Marouane",
        lastName: "Terai",
        created: new Date(),
        modified: new Date()
      },
      {
        id: 2,
        email: "renaud.angles@ynov.com",
        password: "1234",
        firstName: "Renaud",
        lastName: "Angles",
        created: new Date(),
        modified: new Date()
      }
    ];

    const comments = [
      {
        id: 1,
        content: "Ceci est un commentaire 1",
        user: {
          id: 1
        },
        articleId: 1,
        created: new Date(),
        modified: new Date()
      },
      {
        id: 2,
        content: "Ceci est un commentaire 2",
        user: {
          id: 2
        },
        articleId: 3,
        created: new Date(),
        modified: new Date()
      },
      {
        id: 3,
        content: "Ceci est un commentaire 3",
        user: {
          id: 1
        },
        articleId: 3,
        created: new Date(),
        modified: new Date()
      },
      {
        id: 4,
        content: "Ceci est un commentaire 4",
        user: {
          id: 2
        },
        articleId: 4,
        created: new Date(),
        modified: new Date()
      },
    ]
    return { articles, users, comments };
  }
}